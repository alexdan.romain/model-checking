
# CTL Model checker

The aim of the project is to implement a CTL model checker which takes two input types, one being a Kripke structure that describes the behaviour of the system and another a CTL formula. Here are the following project’s challenges:
- **The creation a custom CTL language**
- **The parsing and the interpretation of the CTL language**
- **The importation and the deserialization of a Kripke structure**
- **The management of several algorithms in order to compute CTL formulas**
- **The validation of a CTL formula for a given Kripke structure.**

# Project structure

>Only important files are listed and keep in mind this is a non exhaustive template of the structure project.
```raw
.
├── examples - Repertory containing all prebuild graph to be tested
│   ├── CTL_cases.txt - text file containing examples of formula with their syntaxt.
│   ├── Graph_1 - repertory for one graph case.
│   │   ├── graph.txt - example of graph to import while running the program.
│   │   ├── formula_graph.png - correct result for a given formula on the current graph.
│   │   └── ...
│   └── ...
├── models - Repertory containing all model files
│   ├── model.py — model file.
│   └── ...
├── tests - Repertory containing all tests files of the application.
│   ├── main.py - main test file used to launch all test.
│   ├── test_file.py - test file.
│   └── ...
├── utils - Repertory containing all utility files.
│   ├── constants.py - constant file.
│   ├── helper.py - helper file containing several functions used by the program.
│   └── ...
├── main.py - main file of the project.
└── README.md - readme file of the project.
```
# CTL formula syntaxe

The following part describe how the program will interprete formulas. You should lean on those in order for the program to provide good results:

|OPERATOR|SYNTAX|
|-|-|
|**Logic :**|
|And|`AND`|
|Or|`OR`|
|Not|`NOT`|
|Implies|`=>`|
|Equivalent|`<=>`|
|**Temporal :**|
|At least one path|`E`|
|All path|`A`|
|Next|`X`|
|Always|`G`|
|Future|`F`|
|Until|`U`|
|**Other :**|
|Atomic proposition|`[name of atomic prop]` (e.g : `req1`)|
|True|`TRUE`|

In order for the program to fully recognize and determine the order of priority of different operations in your formulas, you need to use parenthesis `(` and spaces ` `.

Here is a good example of formula:
- `AXreq1` should be written `A ( X ( req1 ) )`
- `AXreq1Ureq2` should be written `A ( X ( req1 U req2 ) )`

>NOTE: the program does not allow to enter any non-CTL fomulas and you will be informed in this case.

## Requirements to interpret a graph

In order for the program to build a graph from an external file, you have to follow the following format rules:
- File must be a text file (`.txt`)
- File must follows:

|LINE|SYNTAX|
|-|-|
|**1**|`[Number of states] [Number of atomic propositions]`|
|**2**|`[Node name] [Number of atomic propoisitions] [List of atomic propositions]` (e.g: `S0 2 p,q`)|
|...||
|.. n||
|**3**|`[Initial node name]`|
|...||
|.. n||
|**4**|`[Targeted node name] [Input node name]` (e.g: `S0 S1` Here S1 <- S0)|
|...||
|.. n||

Here is a full example:
```mermaid
graph LR
0((0)) --> 1((1))
0((0)) --> 2((2))
1((1)) --> 4((4))
1((1)) --> 3((3))
2((2)) --> 3((3))
2((2)) --> 5((5))
3((3)) --> 6((6))
3((3)) --> 7((7))
4((4)) --> 6((6))
4((4)) --> 0((0))
5((5)) --> 7((7))
5((5)) --> 0((0))
6((6)) --> 2((2))
7((7)) --> 1((1))
```
>NOTE: if a state doesn't have any atomic proposition, assign it an unused atomic proposition such as "none".

|NODE|ATOMIC PROPOSITIONS|
|-|-|
|0|end1, end2|
|1|req1, req2|
|2|req2, end1|
|3|req1, req2|
|4|cs1|
|5|cs2|
|6|req2, cs1|
|7|req1, cs2|

Here is what the .txt file should look like:
```
8 8
0 2 end1,end2
1 2 req1,end2
2 2 req2,end1
3 2 req1,req2
4 1 cs1
5 1 cs2
6 2 req2,cs1
7 2 req1,cs2
0
1 0
2 0
4 1
3 1
3 2
5 2
6 3
7 3
6 4
0 4
7 5
0 5
2 6
1 7
```

## Requirements and dependencies

Mandatory requirements:

- Python v3.10 or higher

## How to run the program

First clone the project using `git clone https://gitlab.com/alexdan.romain/model-checking.git`.

In order to run the program and use it, you first need to satisfy all **requirements and dependencies mandatory** for the project to run.

Then execute the main program (main.py at the root of the project).

Once executed, a menu will appear in the console and follow printed instructions:
- 0 : tests CTL formulas on a loaded graph
	-  enter the formula according to the **CTL formula syntax**
	- "exit()" to come back to the previous menu
- 1 : load a graph 
	- choose a file containing all **requirements about the graph** you want to load and test with formulas
- 2 : to exit the program

In the "examples" folder's root you have a file named "CTL_cases.txt" where you can find examples of formulas for each graph example the folder contains.
Indeed, there is several other folders named "...Graph..." containing for each :
- a .txt file which you can import in ordrer to load a graph through the console menu
- several images with correct results for each formulas example you can find in "CTL_cases.txt" for the current graph

>NOTE: The program returns a list of satisfied nodes for every graph which does not exceed 20 nodes in total and tells if the graph satisfies the given formula.

## How to run tests

First clone the project using `git clone https://gitlab.com/alexdan.romain/model-checking.git`.

Then run `python -m unittest -v tests/main.py`.

What is tested ?

We have several tests cases in order to maintain regression and to ensure a certain code quality.
- Errors test cases with error message tests
- Model instantiation tests & models methods tests with error message tests

Currently, we have 113 passing tests

## Authors

[Tristan Clémenceau](https://gitlab.com/tristann)

[Seedy JOBE](https://gitlab.com/seedyjobe)

[Alex Romain](https://gitlab.com/alexdan.romain)

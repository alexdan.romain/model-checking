import re
from typing import List
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils.constants import DEFAULT

####################################################################
#NODE#
####################################################################

class Node(object):
    """
    This class is used to represent a node of a Kripke structure.

    Attributes:
        - `label` (str): It is the label of the node.
        - `atomicProps` (List[str]): It is an array containing all the atomic proposition of the current node.
        - `input` (List[Node]): It is the list of the node which are arriving into the current node.
        - `output` (List[Node]): It is the list of the node where the current node point towards.

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        node: Node = Node("S1")
    """

    def __init__(self, label: str):
        self.label: str = label
        self.atomicProps: List[str] = []
        self.input: List[Node] = []
        self.output: List[Node] = []

    def print(self, previousNodes: List, level: int):
        """Format this node and its childs recursively.

        Args:
            previousNodes (List[Node]): Previous nodes which has been already "printed"
            level (int): The actual level of the tree node

        Returns:
            str: The tree node's customized representation used to debug or just have a visual rep
        """
        tempNodes = previousNodes
        tempNodes.append(self)

        temp = DEFAULT["MSG"]["NODEOBJ_PRINT_INFO"].format(
            self.label, self.str_output(), self.str_atomicProps()
        )

        for child in self.output:
            if child in tempNodes is False:
                for x in range(0, level):
                    temp += "\t"
                temp += "\t" + child.print(tempNodes, level + 1)

        return temp

    def __eq__(self, node):
        areObjValid = isinstance(self, Node) and isinstance(node, Node)

        if(not areObjValid):
            return False

        if(not (self.get_label() == node.get_label())):
            return False

        return True

    def __str__(self):
        return DEFAULT["MSG"]["NODEOBJ_STR_INFO"].format(
            self.str_input(), self.label, self.str_output(), self.str_atomicProps()
        )

    # Label
    def set_label(self, label):
        """The label's setter.

        Args:
            label (str): The new name of the node
        """
        self.label = label

    def get_label(self):
        """The label's getter

        Returns:
            str: The node's label
        """
        return self.label

    # AtomicProps
    def get_atomicProps(self):
        """The atomicProps's getter.

        Returns:
            List[str]: The node's atomic propositions list
        """
        return self.atomicProps

    def add_atomicProp(self, atomicProp: str):
        """This function allows to add a new atomic proposition to the node.

        Args:
            atomicProp (str): The new atomic proposition to add
        """
        if atomicProp in self.atomicProps:
            raise ValueError(DEFAULT["MSG"]["NODEOBJ_ADDATOMICPROP"])
        else:
            self.atomicProps.append(atomicProp)

    # Input
    def set_input(self, input):
        """The input's setter

        Args:
            input (List[Node]): The new list of nodes
        """
        self.input = input

    def get_input(self):
        """The input's getter.

        Returns:
            List[Node]: The node's input list
        """
        return self.input

    def add_input(self, input):
        """This function allows to add new inputs to a node.

        Args:
            input (Node): The new input to be added

        Raises:
            ValueError: Can't add a node which already exists
        """
        if input in self.input:
            raise ValueError(DEFAULT["MSG"]["NODEOBJ_ADDINPUT"])
        else:
            self.input.append(input)

    # Output
    def set_output(self, output):
        """The output's setter.

        Args:
            output (List[Node]): The new node's output list
        """
        self.output = output

    def get_output(self):
        """The output's getter.

        Returns:
            List[Node]: The node's output list
        """
        return self.output

    def add_output(self, output):
        """This function allows to add a new output node to the actual node.

        Args:
            output (Node): The new node to be added to the list

        Raises:
            ValueError: Can't add an already existing node twice
        """
        if output in self.output:
            raise ValueError(DEFAULT["MSG"]["NODEOBJ_ADDOUTPUT"])
        else:
            self.output.append(output)

    def str_input(self):
        """This function returns a string representation of the label's list

        Returns:
            str: The node's label custom str formatted like '[label, label, ...]'
        """
        temp = "["
        for idx, val in enumerate(self.input):
            temp += (", " if (idx != 0) else "") + val.get_label()
        return temp + "]"

    def str_output(self):
        """This function returns a string representation of the output's list

        Returns:
            str: The node's output custom str formatted like '[output, output, ...]'
        """
        temp = "["
        for idx, val in enumerate(self.output):
            temp += (", " if (idx != 0) else "") + val.get_label()
        return temp + "]"

    def str_atomicProps(self):
        """This function returns a string representation of the input's list

        Returns:
            str: The node's input custom str formatted like '[input, input, ...]'
        """
        temp = "["
        for idx, val in enumerate(self.atomicProps):
            temp += (", " if (idx != 0) else "") + val
        return temp + "]"

####################################################################
#GRAPH#
####################################################################

class Graph(object):
    """
    This class is used to represent the Kripke structure.

    Attributes:
        - `initials` (List[Node]): It is the list of the initials points.
        - `nodes` (List[Node]): It is the list of all the node of the current kripke structure.
        - `atomicProps` (List[str]): It is all the possible atomic proposition of the graph.

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        graph: Graph = Graph()
    """
    def __init__(self):
        self.initials: List[Node] = []
        self.nodes: List[Node] = []
        self.atomicProps: List[str] = []

    # Nodes
    def get_initials(self):
        """The getter of the graph's initials list.

        Returns:
            List[Node]: The graph's initial nodes list
        """
        return self.initials

    def add_initial(self, initial: Node):
        """This function allows to add a new initial node from a given node.

        Args:
            initial (Node): The new initial node to be added

        Raises:
            ValueError: Can't add an already existing node
        """
        if initial in self.initials:
            raise ValueError(DEFAULT["MSG"]["GRAPHOBJ_ADDINITIALS"])
        else:
            self.initials.append(initial)

    # Nodes
    def get_nodes(self):
        """The getter of the list of nodes

        Returns:
            List[Node]: The nodes list of the graph
        """
        return self.nodes

    def get_node(self, label: str):
        """This function check if a node exists in the graph's nodes list from a given node label and return it else return False.

        Returns:
            Node: The node according to the given label
            or
            False: if this node doesn't exists
        """
        for node in self.get_nodes():
            if(node.label == label):
                return node

        return False

    def add_node(self, node: Node):
        """This function allows to add a new node to the graph.

        Args:
            node (Node): The new node to be added to the graph
        """
        if node in self.nodes:
            raise ValueError(DEFAULT["MSG"]["GRAPHOBJ_ADDNODE"])
        else:
            self.nodes.append(node)

    # AtomicProps
    def get_atomicProps(self):
        """The atomicProps's getter.

        Returns:
            List[str]: The list of atomicProps of the graph
        """
        return self.atomicProps

    def add_atomicProp(self, atomicProp: str):
        """This function allows to add an atomic proposition to the graph.

        Args:
            atomicProp (str): A string corresponding to an atomic proposition
        Raises:
            ValueError: can't have the same atomic proposition twice
        """
        if atomicProp in self.atomicProps:
            raise ValueError(DEFAULT["MSG"]["GRAPHOBJ_ADDATOMICPROP"])
        else:
            self.atomicProps.append(atomicProp)

    def __eq__(self, graph) -> bool:
        areObjValid = isinstance(self, Graph) and isinstance(graph, Graph)

        if(not areObjValid):
            return False

        if(not (set(self.get_atomicProps()) == set(graph.get_atomicProps()))):
            return False

        if(not(set(list(map(lambda el: el.get_label(), self.get_initials()))) == set(list(map(lambda el: el.get_label(), graph.get_initials()))))): return False
        if(not(set(list(map(lambda el: el.get_label(), self.get_nodes()))) == set(list(map(lambda el: el.get_label(), graph.get_nodes()))))): return False

        return True

    def isSatisfied(self, nodes: List[Node]):
        temp: bool = False
        for initNode in self.get_initials():
            if initNode in nodes:
                temp = True

        if temp:
            print(DEFAULT["MSG"]["GRAPHOBJ_ISSATISFIED_TRUE"])
        else:
            print(DEFAULT["MSG"]["GRAPHOBJ_ISSATISFIED_FALSE"])

    def __str__(self):
        return self.nodes.__getitem__(0).print([], 0)

####################################################################
#ALGO#
####################################################################

class Algo:
    """
    This class is used to apply an algorithm on a Graph.

    Attributes:
        - `graph` (List[Node]): It is the graph that is going to be tested with all the available algorithms.

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        algo: Algo = Algo(Graph())
    """
    def __init__(self, kripke: Graph):
        self.graph: Graph = kripke

    # phi
    def checkPhi(self, phi: str):
        marked: List[Node] = []

        # For all the graph's nodes
        for node in self.graph.get_nodes():
            # If the current node's atomic propositions contains Phi then the current node is marked
            if(phi in node.get_atomicProps()):
                marked.append(node)

        return marked

    # -psi
    def checkNegation(self, markedPsi: List[Node]):
        marked: List[Node] = []

        # For all the graph's nodes
        for node in self.graph.get_nodes():
            # If the current node is not in the markedPsi list then the current node is marked
            if(not node in markedPsi):
                marked.append(node)

        return marked

    # psi1 AND psi2
    def checkAnd(self, markedPsi1: List[Node], markedPsi2: List[Node]):
        marked: List[Node] = []

        # For all the graph's nodes
        for node in markedPsi1:
            # If the current marked node is also marked in the second marked list, then the node is marked
            if(node in markedPsi2):
                marked.append(node)

        return marked

    # psi1 OR psi2
    def checkOr(self, markedPsi1: List[Node], markedPsi2: List[Node]):
        marked: List[Node] = []

        # For all the graph's nodes
        for node in markedPsi1:
            # If the current marked node is not marked in the second marked list, then the node is marked
            if(not node in markedPsi2):
                marked.append(node)

        # For all the graph's nodes
        for node in markedPsi2:
            # If the current marked node is not marked in the second marked list and it hasn't been already marked, then the node is marked
            if(not node in markedPsi1 and not node in marked):
                marked.append(node)

        return marked

    # EX(psi)
    def checkNext(self, markedPsi: List[Node]):
        marked: List[Node] = []

        # For all the graph's nodes
        for node in self.graph.get_nodes():
            # If the current node's successor's atomic propositions contains Psi then the current node is marked
            for childNode in node.get_output():
                if(childNode in markedPsi and not node in marked):
                    marked.append(node)

        return marked

    # AX(psi)
    def checkAllPathNext(self, markedPsi: List[Node]):
        # AX(psi) <=> -EX-(psi)

        PSI_NEGATION = self.checkNegation(markedPsi)

        ONEPATH_NEXT = self.checkNext(PSI_NEGATION)

        return self.checkNegation(ONEPATH_NEXT)

    # psi1 U psi2
    def checkUntil(self, markedPsi1: List[Node], markedPsi2: List[Node]):
        marked: List[Node] = []
        seenBefore: List[Node] = []
        lMaj: List[Node] = []

        # For all the graph's nodes
        for node in self.graph.get_nodes():
            # if the current node is in markedPsi2 then the current node is marked
            if(node in markedPsi2):
                lMaj.append(node)

        # While their's still nodes in the L list
        while len(lMaj) != 0:
            # Take out a node from the L list
            currentNode: Node = lMaj.pop()
            # Current node is marked
            if(not currentNode in marked):
                marked.append(currentNode)

            # For all the current node's predecessors
            for childNode in currentNode.get_input():
                # if the current predecessor has not been seen before
                if(not childNode in seenBefore):
                    # Add in the seen before list
                    seenBefore.append(childNode)
                    # if the current predecessor's is in markedPs1
                    if(childNode in markedPsi1):
                        # Add the current predecessor in the L list
                        lMaj.append(childNode)

        return marked

    # A(psi1 U psi2)
    def checkAllPathUntil(self, markedPsi1: List[Node], markedPsi2: List[Node]):
        marked: List[Node] = []
        nb: List[int] = []
        lMaj: List[Node] = []

        # For all the graph's nodes
        for node in self.graph.get_nodes():
            # Compute the current node's degree and store it in nb list
            nb.append(len(node.get_output()))

        # For all the graph's nodes
        for node in self.graph.get_nodes():
            # if the current node is in markedPsi2 then the current node is marked
            if(node in markedPsi2):
                lMaj.append(node)

        # While their's still nodes in the L list
        while len(lMaj) != 0:
            # Take out a node from the L list
            currentNode: Node = lMaj.pop()
            # Current node is marked
            marked.append(currentNode)

            # For all the current node's predecessors
            for idx, childNode in enumerate(currentNode.get_input()):
                # The current predecessor's degree lowered by 1
                nb[self.graph.get_nodes().index(childNode)] -= 1
                # If the current predecessor's degree = 0 and is contained in markedPsi and not already marked
                if(nb[idx] == 0 and childNode in markedPsi1 and not childNode in marked):
                    # Add the current predecessor in the L list
                    lMaj.append(childNode)

        return marked

    # EG(psi)
    def checkAlways(self, markedPsi: List[Node]):
        marked: List[Node] = []
        unmarked: List[Node] = []

        # for all graph's nodes if one doesn't satisfy psi then unmark it | else mark it
        for node in self.graph.get_nodes():
            if not node in markedPsi:
                unmarked.append(node)
            else:
                marked.append(node)

        # for all unmarked nodes
        for node in unmarked:
            # for all current node's predecessors
            for predecessor in node.get_input():
                temp = False
                # for all the predecessor's successors
                for output in predecessor.get_output():
                    # if one of them is marked
                    if(output in marked):
                        temp = True
                # if none of the predecessor's successors is marked and the predecessor was marked
                if not temp and predecessor in marked:
                    # unmark it
                    marked.remove(predecessor)
                    unmarked.append(predecessor)

        return marked

    # AG(phi)
    def checkAllPathAlways(self, markedPhi: List[Node]):
        # AG(phi) <=> -EF(-phi)

        NEGATION_PHI = self.checkNegation(markedPhi)
        FUTURE_NEGATION_PHI = self.checkFuture(NEGATION_PHI)

        return self.checkNegation(FUTURE_NEGATION_PHI)

    # EF(phi)
    def checkFuture(self, markedPhi: List[Node]):
        # EF(phi) <=> E(true U p)
        return self.checkUntil(self.graph.get_nodes(), markedPhi)

    # AF(phi)
    def checkAllPathFuture(self, markedPhi: List[Node]):
        # AF(phi) <=> -EG(-phi)

        NEGATION_PHI = self.checkNegation(markedPhi)
        ALWAYS_NEGATION_PHI = self.checkAlways(NEGATION_PHI)

        return self.checkNegation(ALWAYS_NEGATION_PHI)

    # E(psi1 => psi2)
    def checkImplies(self, markedPhi1: List[Node], markedPhi2: List[Node]):
        marked: List[Node] = []

        for node in self.graph.get_nodes():
            if node in markedPhi1:
                temp = True
                for successor in node.get_output():
                    if not successor in markedPhi2:
                        temp = False
                if temp:
                    marked.append(node)

        return marked

####################################################################
#TOKEN#
####################################################################

class Token(object):
    """
    This class is used to represent the token that are going to be used in the parser.

    Attributes:
        - `type` (string): It is the type of the token (e.g : "NEXT","ATOMIC",...).
        - `value` (string): It is the value of the token (e.g : "req1","(","A",...).

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        token: Token = Token("ATOMIC","req1")
    """

    def __init__(self, type: str, value: str):
        self.type = type
        self.value = value

    def __str__(self):
        return 'Token({type}, {value})'.format(
            type=self.type,
            value=repr(self.value)
        )

    def __repr__(self):
        return self.__str__()

    def __eq__(self, token):
        areObjValid = isinstance(self, Token) and isinstance(token, Token)

        if(not areObjValid):
            return False

        if(not (self.type == token.type)):
            return False
        if(not (self.value == token.value)):
            return False

        return True

####################################################################
#TREENODE#
####################################################################

class TreeNode(object):
    """
    This class is used to represent a tree that are going to be used in the parser and interpreter.

    Attributes:
        - `root` (Token): It is the root of the tree and it must be a Token (e.g : Token("ATOMIC","req1") ,...).
        - `left` (None | TreeNode): It is the left child of the root node, it can be none or a TreeNode.
        - `right` (None | TreeNode): It is the right child of the root node, it can be none or a TreeNode.

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        treeNode: TreeNode = TreeNode(Token("ATOMIC","req1"))
    """

    def __init__(self, root: Token = None, left=None, right=None):
        self.root = root
        self.left = left
        self.right = right

    def __str__(self):
        return '(Root = {}) \n\t (left = {} \t right = {})'.format(self.root, self.left, self.right)

    def __eq__(self, treeNode):
        areObjValid = isinstance(
            self, TreeNode) and isinstance(treeNode, TreeNode)

        if(not areObjValid):
            return False

        if(not (self.root == treeNode.root)):
            return False
        if(not (self.left == treeNode.left)):
            return False
        if(not (self.right == treeNode.right)):
            return False

        return True

    def __repr__(self):
        return self.__str__()

####################################################################
#LEXER#
####################################################################

class Lexer(object):
    """
    This class is used to transform a user input(ctl formulae) into a list of token.

    Attributes:
        - `text` ([str]): It is the user input recieved.

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        lexer: Lexer = Lexer("A E req1")
    """

    def __init__(self, text):
        self.text: List[str] = text

    def error(self):
        raise Exception('Invalid character')

    def isAtomicProp(self, word):
        """
        This method is used to verrify if the word can be an atomic proposition or not.

        Args:
            `word` (str): It is a word that is comming from the splitted user input.

        Returns:
            bool: It return `True` if the word look likes an atomic proposition otherwize it returns `False`.
        """
        alphanum = re.search("^[a-zA-Z0-9]+$", word)
        operator = word in ['OR', 'AND', 'NOT', 'X',
                            'G', 'F', 'U', 'A', 'E', 'TRUE', 'FALSE']

        return (alphanum and operator != True)

    def tokenizer(self):
        """
        This function is used to transform a string into token. It is also called a scanner or a tokenizer.

        Returns:
            [Token]: It returns an array of Token based on the rules or an empty array.
        """
        if(self.text is not None):
            tokens = []
            parts = str(" ".join(str(self.text).split())).split(" ")

            for element in parts:
                if(self.isAtomicProp(element)):
                    tokens.append(
                        Token(DEFAULT["TOKENS"]['ATOMICPROP'], element))
                # PAR
                if(element == DEFAULT["TOKENS"]['PAR']['LEFT']):
                    tokens.append(Token('PARLEFT', element))

                if(element == DEFAULT["TOKENS"]['PAR']['RIGHT']):
                    tokens.append(Token('PARRIGHT', element))
                # LOGIC
                if(element == DEFAULT["TOKENS"]['OPERATORS']['LOGIC']['AND']):
                    tokens.append(Token('AND', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['LOGIC']['OR']):
                    tokens.append(Token('OR', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['LOGIC']['NOT']):
                    tokens.append(Token('NOT', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['LOGIC']['IMPLIES']):
                    tokens.append(Token('IMPLIES', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['LOGIC']['EQUIVALENT']):
                    tokens.append(Token('EQUIVALENT', element))
                # TEMPORAL
                if(element == DEFAULT["TOKENS"]['OPERATORS']['TEMPORAL']['NEXT']):
                    tokens.append(Token('NEXT', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['TEMPORAL']['ALWAYS']):
                    tokens.append(Token('ALWAYS', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['TEMPORAL']['ONEPATH']):
                    tokens.append(Token('ONEPATH', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['TEMPORAL']['FUTURE']):
                    tokens.append(Token('FUTURE', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['TEMPORAL']['UNTIL']):
                    tokens.append(Token('UNTIL', element))

                if(element == DEFAULT["TOKENS"]['OPERATORS']['TEMPORAL']['ALLPATH']):
                    tokens.append(Token('ALLPATH', element))

                if(element == DEFAULT["TOKENS"]['TRUE']):
                    tokens.append(Token('TRUE', element))

                if(element == DEFAULT["TOKENS"]['FALSE']):
                    tokens.append(Token('FALSE', element))
            return tokens
        else:
            return Token(DEFAULT["TOKENS"]['EOF'], None)

####################################################################
#PARSER#
####################################################################

class Parser(object):
    """
    This class is used to transform a list of Token into an AST(Async Syntax Tree).

    Attributes:
        - `lexer` ([Token]): It is the list of tokens .
        - `counter` (int): It is a cursor that is used to move throught the array of tokens.
        - `current_token` (Token): It is the current token that is processed by the parser to build the AST.

    Args:
        - `object` (object): It is a way to extend our class to the base python class object

    Examples:
        parser: Parser = Parser([Token("ATOMIC","req1")])
    """

    def __init__(self, lexer):
        self.lexer: List[Token] = lexer
        self.counter = 0
        self.current_token = self.lexer[self.counter]

    def error(self):
        raise Exception('Invalid syntax')

    def moveCursor(self):
        """
        This method is used to move the cursor based on the lexer length and the counter value.
        """
        if(self.counter < len(self.lexer) - 1):
            self.counter += 1
            self.current_token = self.lexer[self.counter]

    def primary(self):
        """
        This method is used to return a TreeNode based on the tpe of the expression.

        `primary ->  "TRUE" | "FALSE"  | "ATOMPROP" | '(' expression() ')'`

        Returns:
            TreeNode: It return a TreeNode.
        """
        if (self.current_token.type == "PARLEFT"):

            self.moveCursor()
            node = self.expression()
            self.moveCursor()

            return node
        else:
            token = self.current_token
            self.moveCursor()
            return TreeNode(token)

    def basic(self):
        """
        This method is used to return a TreeNode based on the tpe of the expression.

        `basic ->  unary() (("AND" | "OR" | "=>" | "<=>" ) unary())* | ("NOT") unary()`

        Returns:
            TreeNode: It return a TreeNode.
        """
        if(self.current_token.type == "NOT"):
            token = self.current_token
            self.moveCursor()

            return TreeNode(token, self.unary())
        else:
            node = self.unary()

            while(self.current_token.type in ("AND", "OR", "IMPLIES", "EQUIVALENT")):
                token = self.current_token
                self.moveCursor()

                node = TreeNode(token, node, self.unary())
            return node

    def binary(self):
        """
        This method is used to return a TreeNode based on the tpe of the expression.

        `binary ->  basic() (("U") basic() )*`

        Returns:
            TreeNode: It return a TreeNode.
        """
        node = self.basic()

        while(self.current_token.type == "UNTIL"):
            token = self.current_token
            self.moveCursor()

            node = TreeNode(token, node, self.basic())

        return node

    def unary(self):
        """
        This method is used to return a TreeNode based on the tpe of the expression.

        `unary ->  ("F" | "G" | "X" ) + binary() | primary()`

        Returns:
            TreeNode: It return a TreeNode.
        """
        if (self.current_token.type in ("FUTURE", "ALWAYS", "NEXT")):

            token = self.current_token
            self.moveCursor()

            return TreeNode(token, self.binary())
        else:
            return self.primary()

    def ctl(self):
        """
        This method is used to return a TreeNode with either an A or an E as a root.

        `ctl -> ("A" | "E") + unary()`

        Returns:
            TreeNode: It return a TreeNode.
        """
        if(self.current_token.type in ("ALLPATH", "ONEPATH")):

            token = self.current_token
            self.moveCursor()

            return TreeNode(token, self.unary())

    def expression(self):
        """
        This method is used to determine if the current expression is a binary expression or a CTL expression.

        `expression -> ctl() | binary()`

        Returns:
            TreeNode: It return either a TreeNode.
        """
        if(self.current_token.type in ("ALLPATH", "ONEPATH")):
            return self.ctl()
        else:
            return self.binary()

    def buildAST(self):
        """
        This function will be used to create an Abstract Syntax Tree based on the lexer recursively.

        Returns:
            TreeNode: The AST based on the lexer text will be returned.
        """
        return self.expression()


####################################################################
#INTERPRETER#
####################################################################

class Interpreter(object):
    """
    This class is used to interpret an Abstract Syntax Tree (here a treeNode), to compute operators running through it and to provide their result.

    Attributes:
        - `treeNode` (TreeNode): It is the AST in an interpretable form (TreeNode).
        - `algo` (Algo): It is the algorithm class which allows to compute different algorithms for a given Kripke Structure.

    Examples:
        interpreter: Interpreter = Interpreter(TreeNode(Token("ONEPATH", "E"), TreeNode(Token("NEXT", "X"), TreeNode(Token("ATOMIC", "req1"))), None), Algo(givenGraph))
    """

    def __init__(self, treeNode: TreeNode, algo: Algo, formula: str):
        self.algo = algo
        self.treeNode = treeNode
        self.formula = formula

    def raiseError(self, missingTemp: bool = False):
        if missingTemp:
            raise ValueError(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_ATOMIC_ERROR'].format(self.formula))
        else:
            raise ValueError(DEFAULT["MSG"]['INTERPRETEROBJ_RAISEERROR'].format(self.formula))

    def visit(self, treeNode: TreeNode, parentToken: str = None):
        """This method is used to run through an AST or TreeNode recursively and to interpret what algorithm should be used.

        Args:
            treeNode (TreeNode): The current AST (recursively going through the AST from the top)
            parentToken (str, optional): The previous identified parent Token that helps to identify what algorithm should be used with A and E operators. Defaults to None.

        Returns:
            List[Node]: The list of nodes that verify the given formula (in AST form)
        """
        if treeNode.root.type == 'AND':
            if parentToken in DEFAULT["RAISED"]:
                return self.algo.checkAnd(self.visit(treeNode.left, 'AND'), self.visit(treeNode.right, 'AND'))
            else:
                self.raiseError(True)
        elif treeNode.root.type == 'OR':
            if parentToken in DEFAULT["RAISED"]:
                return self.algo.checkOr(self.visit(treeNode.left, 'OR'), self.visit(treeNode.right, 'OR'))
            else:
                self.raiseError(True)
        elif treeNode.root.type == 'NOT':
            if parentToken in DEFAULT["RAISED"]:
                return self.algo.checkNegation(self.visit(treeNode.left, 'NOT'))
            else:
                self.raiseError(True)
        elif treeNode.root.type == 'IMPLIES':
            if parentToken in DEFAULT["RAISED"]:
                return self.algo.checkImplies(self.visit(treeNode.left, 'IMPLIES'), self.visit(treeNode.right, 'IMPLIES'))
            else:
                self.raiseError(True)
        elif treeNode.root.type == 'EQUIVALENT':
            raise ValueError(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_EQUI_ERROR'])

        # A/E LOGIC
        elif treeNode.root.type == 'ALLPATH':
            return self.visit(treeNode.left, 'ALLPATH')

        elif treeNode.root.type == 'ONEPATH':
            return self.visit(treeNode.left, 'ONEPATH')

        # NEXT LOGIC
        elif treeNode.root.type == 'NEXT':
            if parentToken == 'ALLPATH':
                return self.algo.checkAllPathNext(self.visit(treeNode.left, 'NEXT'))
            elif parentToken == 'ONEPATH':
                return self.algo.checkNext(self.visit(treeNode.left, 'NEXT'))
            else:
                self.raiseError()

        # ALWAYS LOGIC
        elif treeNode.root.type == 'ALWAYS':
            if parentToken == 'ALLPATH':
                return self.algo.checkAllPathAlways(self.visit(treeNode.left, 'ALWAYS'))
            elif parentToken == 'ONEPATH':
                return self.algo.checkAlways(self.visit(treeNode.left, 'ALWAYS'))
            else:
                self.raiseError()

        # FUTURE LOGIC
        elif treeNode.root.type == 'FUTURE':
            if parentToken == 'ALLPATH':
                return self.algo.checkAllPathFuture(self.visit(treeNode.left, 'FUTURE'))
            elif parentToken == 'ONEPATH':
                return self.algo.checkFuture(self.visit(treeNode.left, 'FUTURE'))
            else:
                self.raiseError()

        # UNTIL LOGIC
        elif treeNode.root.type == 'UNTIL':
            if parentToken == 'ALLPATH':
                return self.algo.checkAllPathUntil(self.visit(treeNode.left, 'UNTIL'), self.visit(treeNode.right, 'UNTIL'))
            elif parentToken == 'ONEPATH':
                return self.algo.checkUntil(self.visit(treeNode.left, 'UNTIL'), self.visit(treeNode.right, 'UNTIL'))
            else:
                self.raiseError()

        # ATOMICPROP LOGIC
        elif treeNode.root.type == 'ATOMIC':
            if parentToken in DEFAULT["RAISED"]:
                return self.algo.checkPhi(treeNode.root.value)
            else:
                self.raiseError(True)

        else:
            raise ValueError(
                DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_TOKEN_ERROR'].format(treeNode))

    def printTree(self):
        printTree(self.treeNode)

    def interpret(self):
        return self.visit(self.treeNode)


def displayOperators():
    """
    This method is used to inform the user about the CTL operators available.
    """
    print("/!\ All the operators must be in Uppercase.")
    print("Logic operators :")
    print(DEFAULT["TOKENS"]['OPERATORS']["LOGIC"])
    print("Temporal operators :")
    print(DEFAULT["TOKENS"]['OPERATORS']["TEMPORAL"])


def printTree(node: TreeNode, level=0):
    """
    This method is used to print a TreeNode horizontally.

    Example:
    tree: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("UNTIL", "U"),TreeNode(Token("ATOMIC", "req1")),TreeNode(Token("ATOMIC", "cs1"))),None) # CTL : E ( req1 U req2 )

    printTree(tree)

    Args:
        `node` (TreeNode): It is the treeNode we wants to display.
        `level` (int, optional): It is the current level on the tree. Defaults to 0.
    """
    if node != None:
        printTree(node.right, level + 1)
        print(' ' * 4 * level + '->', node.root)
        printTree(node.left, level + 1)

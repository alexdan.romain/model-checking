from utils.constants import DEFAULT

from tkinter import filedialog
from typing import List
import tkinter as tk
import re
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from models import Graph, Node

def isfloat(value):
    """
    This method verifies if a value can be a float or not.

    Args:
        value (any): this value will be tested to see if it can be tranformed into float.

    Returns:
        `bool`: If the value can be converted the result will be `True` otherwise the result will be `False`
    """
    try:
        float(value)
        return True
    except ValueError:
        return False


def isInt(value):
    """
    This method verifies if a value can be an int or not.

    Args:
        value (any): this value will be tested to see if it can be tranformed into int.

    Returns:
        `bool`: If the value can be converted the result will be `True` otherwise the result will be `False`
    """
    try:
        int(value)
        return True
    except ValueError:
        return False


def openFile():
    """
    This function is used to open a file that match the allowed files types.

    Returns:
        `str`: It returns the path of the file the user want to open
    """

    root = tk.Tk()
    root.withdraw()
    root.call('wm', 'attributes', '.', '-topmost', True)

    path = filedialog.askopenfilename(
        filetypes=DEFAULT["FILE_TYPE"], initialdir="./examples")

    if(path == ""):
        raise ValueError(DEFAULT["MSG"]["OPENFILE_ERROR"])

    return path


def readFile(filePath: str):
    """
    This function is used to read all the content of a file based on a `filePath` given in argument.

    Args:
        filePath (str): It is the path of the file we want to read the content.

    Returns:
        [str]: It return an array of str where all the line are in it.
    """

    if(filePath == ""):
        raise ValueError(DEFAULT["MSG"]["READFILE_FILEPATH_ERROR"])

    ans = open(filePath, 'r').readlines()

    if(ans == []):
        raise ValueError(DEFAULT["MSG"]["READFILE_EMPTYFILE_ERROR"])

    return ans


def isFirstLineValid(line: str):
    """
    This Function is used to verify if the first line of the file is correctly formated.

    Args:
        line (str): It is the first line we are going to verify.

    Returns:
        `bool`: It represents the summ of two boolean operation to verify if the first line is valid or not.
        If the first line is correct the result will be `True` otherwise the result will be `False`.
    """
    params = line.split(" ")

    if(len(params) != 2):
        return False

    okTypeParam = True if (isInt(params[0]) and isInt(params[1])) else False

    return okTypeParam


def isSecondLineValid(line: str):
    """
    This Function is used to verify if the line containing a node name and the number of atomic proposition is correctly formated.

    Args:
        line (str): It is the line we are going to verify.

    Returns:
        `bool`: It represents the summ of two boolean operation to verify if the line is valid or not.
        If the line is correct the result will be `True` otherwise the result will be `False`.
    """
    params = line.split(" ")

    if(len(params) != 3):
        return False

    okTypeParam = True if (
        re.search(DEFAULT["REGEX"], params[0]) and isInt(params[1])) else False

    return okTypeParam


def isThirdLineValid(line: str):
    """
    This Function is used to verify if the line containing a node name is correctly formated.

    Args:
        line (str): It is the line we are going to verify.

    Returns:
        `bool`: It represents the summ of two boolean operation to verify if the line is valid or not.
        If the line is correct the result will be `True` otherwise the result will be `False`.
    """
    params = line.split(" ")

    if(len(params) != 1):
        return False

    okTypeParam = True if (
        re.search(DEFAULT["REGEX"], params[0])) else False

    return okTypeParam


def isForthLineValid(line: str):
    """
    This Function is used to verify if the line containing two node name is correctly formated.

    Args:
        line (str): It is the line we are going to verify.

    Returns:
        `bool`: It represents the summ of two boolean operation to verify if the line is valid or not.
        If the line is correct the result will be `True` otherwise the result will be `False`.
    """
    params = line.split(" ")

    if(len(params) != 2):
        return False

    okTypeParam = True if (re.search(
        DEFAULT["REGEX"], params[0]) and re.search(DEFAULT["REGEX"], params[1])) else False

    return okTypeParam


def deserialization(content: List[str]):
    """
    This method will be used to deserialized all the content given in argument.

    Args:
        content ([str]): It represent the array of lines.
    """

    if(len(content) < 4):
        raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_CONTENTLENGTH_ERROR"])

    valid = True
    count = 0

    deserialization = {
        "nbState": 0,
        "nbPropTotal": 0,
    }

    constructedGraph = Graph()

    # Traitement for first line
    if(isFirstLineValid(content[count]) != True):
        raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_LINEFORMAT_ERROR"].format(
            count, content[count].removesuffix("\n")))
    else:
        parts = str(content[count]).removesuffix("\n").split(" ")

        deserialization["nbState"] = parts[0]
        deserialization["nbPropTotal"] = parts[1]

        count += 1

    # Traitement for second line
    while valid == True:
        if(isSecondLineValid(content[count])):
            parts = str(content[count]).split(" ")
            props = str(parts[2]).removesuffix("\n").split(",")

            # verification des props
            if(len(props) < 1):
                raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_SECONDLINE_ERROR"].format(
                    count, content[count].removesuffix("\n")))
            else:
                tempNode = Node(parts[0])

                for el in props:
                    if((el in constructedGraph.get_atomicProps()) != True):
                        constructedGraph.add_atomicProp(el)

                    tempNode.add_atomicProp(el)

                constructedGraph.add_node(tempNode)
            count += 1
        else:
            if(isThirdLineValid(content[count])):
                valid = not valid
            else:
                raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_LINEFORMAT_ERROR"].format(
                    count, content[count].removesuffix("\n")))

    valid = not valid

    # Traitement for third line
    while valid == True:
        if(isThirdLineValid(content[count])):
            if(constructedGraph.get_node(str(content[count]).removesuffix("\n")) != False):
                constructedGraph.add_initial(constructedGraph.get_node(
                    str(content[count]).removesuffix("\n")))
            else:
                raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_THIRDLINE_ERROR"].format(
                    count, content[count].removesuffix("\n")))
            count += 1
        else:
            if(isForthLineValid(content[count])):
                valid = not valid
            else:
                raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_LINEFORMAT_ERROR"].format(
                    count, content[count].removesuffix("\n")))

    valid = not valid

    # Traitement for fourth line
    while valid == True:
        if(count < len(content) and isForthLineValid(content[count])):
            parts = str(content[count]).removesuffix("\n").split(" ")
            if(len(parts) != 2):
                raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_FORTHLINEFORMAT_ERROR"].format(
                    count, content[count].removesuffix("\n")))
            else:
                if(constructedGraph.get_node(parts[1]) == False):
                    raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_FORTHLINENODE_ERROR"].format(
                        count, content[count].removesuffix("\n"), parts[1]))
                if(constructedGraph.get_node(parts[0]) == False):
                    raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_FORTHLINENODE_ERROR"].format(
                        count, content[count].removesuffix("\n"), parts[0]))

                startingNode = constructedGraph.get_node(parts[1])
                arrivalNode = constructedGraph.get_node(parts[0])

                arrivalNode.add_input(startingNode)
                startingNode.add_output(arrivalNode)
            count += 1
        else:
            if(count == len(content)):
                valid = not valid
            else:
                raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_LINEFORMAT_ERROR"].format(
                    count, content[count].removesuffix("\n")))

    if(str(len(constructedGraph.get_atomicProps())) == deserialization["nbPropTotal"] and str(len(constructedGraph.get_nodes())) == deserialization["nbState"]):
        return constructedGraph
    else:
        raise ValueError(DEFAULT["MSG"]["DESERIALIZATION_GRAPH_ERROR"].format(
            0, content[0].removesuffix("\n"), content[0].removesuffix("\n"), len(constructedGraph.get_nodes()), len(constructedGraph.get_atomicProps())))

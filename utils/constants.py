DEFAULT = {
    "TOKENS": {
        "PAR": {
            "LEFT": '(',
            "RIGHT": ')'
        },
        "OPERATORS": {
            "LOGIC": {
                "AND": 'AND',
                "OR": 'OR',
                "NOT": 'NOT',
                "IMPLIES": '=>',
                "EQUIVALENT": '<=>'
            },
            "TEMPORAL": {
                "NEXT": 'X',
                "ALWAYS": 'G',
                "FUTURE": 'F',
                "UNTIL": 'U',
                "ALLPATH": 'A',
                "ONEPATH": 'E'
            }
        },
        "TRUE": "TRUE",
        "FALSE": "FALSE",
        "EOF": 'EOF',
        "ATOMICPROP": "ATOMIC"
    },
    "RAISED": ['AND', 'OR', 'NOT', 'IMPLIES', 'NEXT', 'FUTURE', 'UNTIL', 'ALWAYS'],
    "REGEX": "^[a-zA-Z0-9]+$",
    "FILE_TYPE": [('text files', '*.txt')],
    "MSG": {
        "USERCHOICE_INPUT_PART1": "{}'{}' if you want to leave this CLI.\nCTL> ",
        "USERCHOICE_INPUT_PART2": "Please provide a CTL formulae or",
        "LOADGRAPH_INFO": "Graph loaded at path '{}'",
        "LOADGRAPH_ERROR": "An Error occured when loading the graph : [ {} ]",
        "MENUCHOICE_INPUT": "{}\nYour choice in the following array of choices {}: ",
        "MENUCHOICE_ERROR": "Please enter a value included in the following array of choices {}",
        "MAINCTL_INFO": "Leaving CTL.",
        "MAIN_CTL_ERROR": "/!\ You must load a graph before testing CTL formulae.",
        "MAIN_INFO": "Leaving program.",
        "MAIN_MENU_MSG": "[0] - CTL\n[1] - Load graph\n[2] - Exit",
        "OPENFILE_ERROR": "A file need to be provided",
        "READFILE_FILEPATH_ERROR": "A valid path file must be provided",
        "READFILE_EMPTYFILE_ERROR": "Your file can't be empty.",
        "DESERIALIZATION_CONTENTLENGTH_ERROR": "File doesn't have enough information/ line (inserrer erreur plus grande avec un exemple de fichier attendu)",
        "DESERIALIZATION_LINEFORMAT_ERROR": "Error at line {} : '{}' is not well formed.",
        "DESERIALIZATION_SECONDLINE_ERROR": "Error at line {} : '{}' is not well formed. You must add at least one atomic proposition",
        "DESERIALIZATION_THIRDLINE_ERROR": "Error at line {} : '{}' is not well formed. You must add at least one entry point",
        "DESERIALIZATION_FORTHLINEFORMAT_ERROR": "Error at line {} : '{}' is not well formed. You must have two nodes name 'ArrivalNodeName StartNodeName'",
        "DESERIALIZATION_FORTHLINENODE_ERROR": "Error at line {} : '{}' is not well formed. '{}' can't be found in the graph that is currently building.",
        "DESERIALIZATION_GRAPH_ERROR": "Error at line {} : '{}' is not well formed. Your graph doesnt follow the constraints you put in the first line based on the built graph. You marked '{}' it must be '{} {}'.",
        "NODEOBJ_PRINT_INFO": "{} --> {} | AP = {}\n",
        "NODEOBJ_STR_INFO": "{} --> {} --> {} | AP = {}",
        "NODEOBJ_ADDATOMICPROP": "A node can't have the same atomic proposition twice.",
        "NODEOBJ_ADDINPUT": "A node can't have the same input node twice.",
        "NODEOBJ_ADDOUTPUT": "A node can't have the same output node twice.",
        "GRAPHOBJ_ADDNODE": "A graph can't have the same node twice.",
        "GRAPHOBJ_ADDINITIALS": "A node can't have the same initial node twice",
        "GRAPHOBJ_ADDATOMICPROP": "A graph can't have the same atomic proposition twice.",
        "GRAPHOBJ_ISSATISFIED_TRUE": "This Kripke Structure is satisfied by the given formula.",
        "GRAPHOBJ_ISSATISFIED_FALSE": "This kripke structure is not satisfied by the given formula.",
        "INTERPRETEROBJ_RAISEERROR": "This formula => {} is missing a E/A logic and therefore is not a valid CTL formula.",
        "INTERPRETEROBJ_VISIT_EQUI_ERROR": "Operator <=> is not handled yet by the program.",
        "INTERPRETEROBJ_VISIT_ATOMIC_ERROR": "This formula => {} is missing a temporal operator and therefore is not a valid CTL formula.",
        "INTERPRETEROBJ_VISIT_TOKEN_ERROR": "Wrong input format for the interpreter at token {}.",
    }
}

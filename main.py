from utils import DEFAULT, openFile, readFile, deserialization
from typing import List
from models import Algo, Graph, Lexer, Parser, Interpreter, displayOperators

# Const
STOP_VALUE = "exit()"

# Var
stop = False
keyboardExcept = False
graph : Graph = None

# Functions and Methods
def printRes(res):
    for node in res:
        print(node)

def userChoice():
    """
    This function is used to get the `user choice`.

    Returns:
        int : The return value is the `user choice`.
    """
    global stop
    choice = input(DEFAULT["MSG"]["USERCHOICE_INPUT_PART1"].format(DEFAULT["MSG"]["USERCHOICE_INPUT_PART2"], STOP_VALUE))

    if(choice == STOP_VALUE):
        stop = True

    return choice


def loadGraph():
    """
    This method is used to load a graph by showing a explorer windows.
    """
    global graph
    
    try:
        path = openFile()
        content = readFile(path)
        graph = deserialization(content)
        print(DEFAULT["MSG"]["LOADGRAPH_INFO"].format(path))
    except Exception as e:
        print(DEFAULT["MSG"]["LOADGRAPH_ERROR"].format(e.args[0]))

def testCTLonGraph(ctl:str):
    """
    This method is used to verify if the current CTL formula is satified applied on the graph loaded.

    Args:
        `ctl` (str): It is the CTL formulae entered by the user.
    """
    global graph 

    lexer = Lexer(ctl)
    parser = Parser(lexer.tokenizer())
    algo = Algo(graph)
    interpreter = Interpreter(parser.buildAST(), algo, ctl)

    try:
        result = interpreter.interpret()

        if len(graph.get_nodes()) < 20:
            print()
            print("Here is the list of nodes which statisfy the given formula '{}': \n".format(ctl))
            printRes(result)
        print()
        graph.isSatisfied(result)
        print()

    except ValueError as ve:
        print()
        print(ve)
        print()

    
def menuChoice(msg:str,choices: List[str]):
    """
    This function is used to ask a user to provide an input based on a list of choices and a msg.

    Args:
        `msg` (str): It is the message that will be displayed to the user.
        `choices` (List[str]): It is the list of choices that the user can use.

    Raises:
        `ValueError`: Please enter a value included in the array of choices.

    Returns:
        [str|ValueError]: If the value is in the array of choice the value will be returned otherwise a ValueErro will be raised.
    """

    isCorrect = False
    
    while not isCorrect:
        try:
            inp = input(DEFAULT["MSG"]["MENUCHOICE_INPUT"].format(msg,choices))

            if(inp in choices):
                break
            else:
                raise ValueError()
        except ValueError:
            print(DEFAULT["MSG"]["MENUCHOICE_ERROR"].format(choices))
            isCorrect = False
        except KeyboardInterrupt:
            inp = 2
    return inp

def mainCTL():
    """
    This method is used to manage the CTL operations.
    """
    global keyboardExcept
    
    displayOperators()
    
    try:
        ctl = userChoice()
        while stop != True:
            testCTLonGraph(ctl)
            ctl = userChoice()
    except KeyboardInterrupt:
        keyboardExcept = True
        print(DEFAULT["MSG"]["MAINCTL_INFO"])
    finally:
        if(keyboardExcept != True):
            print(DEFAULT["MSG"]["MAINCTL_INFO"])

def main():
    """
    This method is the main menu that is going to manage all the interactions and redirect the user based on his choice.
    """
    global graph 
    
    choice = menuChoice(DEFAULT["MSG"]["MAIN_MENU_MSG"],["0","1","2"])
    stop = True if choice == "2" else False
    
    while stop != True:
        if(choice == "0"):
            if(graph == None):
                print(DEFAULT["MSG"]["MAIN_CTL_ERROR"])
            else:
                mainCTL()
        elif(choice == "1"):
            loadGraph()
        else:
            stop = not stop
            
        if(stop != True):
            choice = menuChoice(DEFAULT["MSG"]["MAIN_MENU_MSG"],["0","1","2"])
            
    print(DEFAULT["MSG"]["MAIN_INFO"])
  
main()

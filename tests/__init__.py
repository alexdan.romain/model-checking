from tests.test_Models import TestNode, TestGraph, TestAlgo, TestLexer, TestParser, TestInterpreter
from tests.test_Helpers import (
    TestDeserialization,
    TestIsFirstLineValid,
    TestIsSecondLineValid,
    TestIsThirdLineValid,
    TestIsForthLineValid,
)

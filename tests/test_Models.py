import unittest

import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils.helper import deserialization, readFile
from models import Node, Graph, Algo, Lexer, Token, Parser, TreeNode, Interpreter
from utils.constants import DEFAULT

#########################
#        [NODE]        #
#########################

class TestNode(unittest.TestCase):
    def testInstanciateNode(self):
        self.node = Node("s1")

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), [])
        self.assertCountEqual(self.node.get_input(), [])
        self.assertCountEqual(self.node.get_output(), [])

    def testAddNodeInput(self):
        self.node = Node("s1")
        self.node.add_input(self.node)

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), [])
        self.assertCountEqual(self.node.get_input(), [self.node])
        self.assertCountEqual(self.node.get_output(), [])

    def testAddAlreadyExistingNodeInput(self):
        self.node = Node("s1")
        self.secondNode = Node("s1")
        self.node.add_input(self.node)
        
        
        with self.assertRaises(ValueError) as context:
            self.node.add_input(self.secondNode)

        self.assertTrue(
            "A node can't have the same input node twice" in str(context.exception))

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), [])
        self.assertCountEqual(self.node.get_input(), [self.node])

    def testAddNodeOutput(self):
        self.node = Node("s1")
        self.node.add_output(self.node)

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), [])
        self.assertCountEqual(self.node.get_input(), [])
        self.assertCountEqual(self.node.get_output(), [self.node])

    def testAddAlreadyExistingNodeOutput(self):
        self.node = Node("s1")
        self.secondNode = Node("s1")
        self.node.add_output(self.node)

        with self.assertRaises(ValueError) as context:
            self.node.add_output(self.secondNode)

        self.assertTrue(
            "A node can't have the same output node twice" in str(context.exception))

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), [])
        self.assertCountEqual(self.node.get_input(), [])
        self.assertCountEqual(self.node.get_output(), [self.node])

    def testAddNodeAtomicProp(self):
        self.node = Node("s1")
        self.node.add_atomicProp("p")

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), ["p"])
        self.assertCountEqual(self.node.get_input(), [])
        self.assertCountEqual(self.node.get_output(), [])

    def testAddAlreadyExistingNodeAtomicProp(self):
        self.node = Node("s1")
        self.node.add_atomicProp("p")

        with self.assertRaises(ValueError) as context:
            self.node.add_atomicProp("p")

        self.assertTrue(
            "A node can't have the same atomic proposition twice" in str(context.exception))

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), ["p"])
        self.assertCountEqual(self.node.get_input(), [])
        self.assertCountEqual(self.node.get_output(), [])

    def testNodeStrOutput(self):
        self.node = Node("s1")
        self.node.add_input(self.node)
        self.node.add_output(self.node)
        self.node.add_atomicProp("p")

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), ["p"])
        self.assertCountEqual(self.node.get_input(), [self.node])
        self.assertCountEqual(self.node.get_output(), [self.node])
        self.assertEqual(self.node.str_output(), "[s1]")

    def testNodeStrInput(self):
        self.node = Node("s1")
        self.node.add_input(self.node)
        self.node.add_output(self.node)
        self.node.add_atomicProp("p")

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), ["p"])
        self.assertCountEqual(self.node.get_input(), [self.node])
        self.assertCountEqual(self.node.get_output(), [self.node])
        self.assertEqual(self.node.str_input(), "[s1]")

    def testNodeStrAtomicProps(self):
        self.node = Node("s1")
        self.node.add_input(self.node)
        self.node.add_output(self.node)
        self.node.add_atomicProp("p")

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), ["p"])
        self.assertCountEqual(self.node.get_input(), [self.node])
        self.assertCountEqual(self.node.get_output(), [self.node])
        self.assertEqual(self.node.str_atomicProps(), "[p]")

    def testNodeStr(self):
        self.node = Node("s1")
        self.node.add_input(self.node)
        self.node.add_output(self.node)
        self.node.add_atomicProp("p")

        self.assertEqual(self.node.get_label(), "s1")
        self.assertCountEqual(self.node.get_atomicProps(), ["p"])
        self.assertCountEqual(self.node.get_input(), [self.node])
        self.assertCountEqual(self.node.get_output(), [self.node])
        self.assertEqual(self.node.__str__(), "[s1] --> s1 --> [s1] | AP = [p]")

    def testNodeEq(self):
        self.node1 = Node("s1")
        self.node2 = Node("s1")
        self.assertEqual(self.node1, self.node2)

    def testNodeNotEq(self):
        self.node1 = Node("s1")
        self.node2 = Node("s2")
        self.assertNotEqual(self.node1, self.node2)

#########################
#        [GRAPH]        #
#########################

courseGraph = Graph()

# Init nodes
node0 = Node("0")
node1 = Node("1")
node2 = Node("2")
node3 = Node("3")
node4 = Node("4")
node5 = Node("5")
node6 = Node("6")
node7 = Node("7")

# Init nodes atomic propositions
node0.add_atomicProp("end1")
node0.add_atomicProp("end2")
node1.add_atomicProp("req1")
node1.add_atomicProp("end2")
node2.add_atomicProp("req2")
node2.add_atomicProp("end1")
node3.add_atomicProp("req1")
node3.add_atomicProp("req2")
node4.add_atomicProp("cs1")
node5.add_atomicProp("cs2")
node6.add_atomicProp("req2")
node6.add_atomicProp("cs1")
node7.add_atomicProp("req1")
node7.add_atomicProp("cs2")

# Init nodes output
node0.set_output([node1, node2])
node1.set_output([node3, node4])
node2.set_output([node3, node5])
node3.set_output([node6, node7])
node4.set_output([node0, node6])
node5.set_output([node0, node7])
node6.set_output([node2])
node7.set_output([node1])

# Init nodes input
node0.set_input([node4, node5])
node1.set_input([node0, node7])
node2.set_input([node0, node6])
node3.set_input([node1, node2])
node4.set_input([node1])
node5.set_input([node2])
node6.set_input([node3, node4])
node7.set_input([node3, node5])

# Adding nodes to the graph
courseGraph.add_node(node0)
courseGraph.add_node(node1)
courseGraph.add_node(node2)
courseGraph.add_node(node3)
courseGraph.add_node(node4)
courseGraph.add_node(node5)
courseGraph.add_node(node6)
courseGraph.add_node(node7)

# Adding atomic props to the graph
courseGraph.add_atomicProp("end1")
courseGraph.add_atomicProp("end2")
courseGraph.add_atomicProp("req2")
courseGraph.add_atomicProp("req1")
courseGraph.add_atomicProp("cs1")
courseGraph.add_atomicProp("cs2")

# Setting the initial node
courseGraph.add_initial(node0)

class TestGraph(unittest.TestCase):
    def testInstanciateGraph(self):
        self.graph = Graph()

        self.assertCountEqual(self.graph.get_nodes(), [])
        self.assertCountEqual(self.graph.get_atomicProps(), [])
        self.assertCountEqual(self.graph.get_initials(), [])

    def testManualInstanciateCourseGraph(self):
        global courseGraph

        self.assertCountEqual(courseGraph.get_nodes(), [node0, node1, node2, node3, node4, node5, node6, node7])
        self.assertCountEqual(courseGraph.get_atomicProps(), ["end1", "end2", "req2", "req1", "cs1", "cs2"])
        self.assertCountEqual(courseGraph.get_initials(), [node0])

    def testManualInstanciateCourseGraphEqualsImportedOne(self):
        global courseGraph

        # Import the graph
        self.importedCourseGraph = deserialization(readFile("./examples/courseGraph/courseGraph.txt"))

        self.assertCountEqual(courseGraph.get_nodes(), self.importedCourseGraph.get_nodes())
        self.assertCountEqual(courseGraph.get_atomicProps(), self.importedCourseGraph.get_atomicProps())
        self.assertCountEqual(courseGraph.get_initials(), self.importedCourseGraph.get_initials())

    def testAddNode(self):
        self.graph = Graph()
        n1 = Node("s1")
        self.graph.add_node(n1)

        self.assertCountEqual(self.graph.get_nodes(), [n1])
        self.assertCountEqual(self.graph.get_atomicProps(), [])
        self.assertCountEqual(self.graph.get_initials(), [])

    def testAddAlreadyExistingNode(self):
        self.graph = Graph()
        n1 = Node("s1")
        self.graph.add_node(n1)

        with self.assertRaises(ValueError) as context:
            self.graph.add_node(n1)

        self.assertTrue(
            "A graph can't have the same node twice" in str(context.exception))

        self.assertCountEqual(self.graph.get_nodes(), [n1])
        self.assertCountEqual(self.graph.get_atomicProps(), [])
        self.assertCountEqual(self.graph.get_initials(), [])

    def testAddAtomicProp(self):
        self.graph = Graph()
        self.graph.add_atomicProp("p")

        self.assertCountEqual(self.graph.get_nodes(), [])
        self.assertCountEqual(self.graph.get_atomicProps(), ["p"])
        self.assertCountEqual(self.graph.get_initials(), [])

    def testAddAlreadyExistingAtomicProp(self):
        self.graph = Graph()
        self.graph.add_atomicProp("p")

        with self.assertRaises(ValueError) as context:
            self.graph.add_atomicProp("p")
        
        self.assertTrue("A graph can't have the same atomic proposition twice" in str(context.exception))
        self.assertCountEqual(self.graph.get_nodes(), [])
        self.assertCountEqual(self.graph.get_atomicProps(), ["p"])
        self.assertCountEqual(self.graph.get_initials(), [])

    def testGetNode(self):
        self.graph = Graph()
        n1 = Node("s1")
        self.graph.add_node(n1)

        self.assertEqual(self.graph.get_node("s1"), n1)
        self.assertCountEqual(self.graph.get_atomicProps(), [])
        self.assertCountEqual(self.graph.get_initials(), [])

    def testGetNonExistingNode(self):
        self.graph = Graph()
        n1 = Node("s1")
        self.graph.add_node(n1)

        self.assertEqual(self.graph.get_node("s2"), False)
        self.assertCountEqual(self.graph.get_atomicProps(), [])
        self.assertCountEqual(self.graph.get_initials(), [])

#########################
#        [ALGO]        #
#########################

algo = Algo(courseGraph)

class TestAlgo(unittest.TestCase):

    def testInstanciateAlgo(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)

    def testCheckPhi(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkPhi("req1"), [node1, node3, node7])

    def testCheckNegationPhi(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkNegation(algo.checkPhi("req1")), [node0, node2, node4, node5, node6])

    def testCheckAnd(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkAnd(algo.checkPhi("req1"), algo.checkPhi("req2")), [node3])

    def testCheckOr(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkOr(algo.checkPhi("req1"), algo.checkPhi("req2")), [node1, node2, node6, node7])

    def testCheckNext(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkNext(algo.checkPhi("req1")), [node0, node1, node2, node3, node5, node7])

    def testCheckUntil(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkUntil(algo.checkPhi("req1"), algo.checkPhi("cs1")), [node1, node3, node4, node6, node7])

    def testCheckAllPathUntil(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkAllPathUntil(algo.checkPhi("req1"), algo.checkPhi("cs1")), [node4, node6])

    def testCheckImplies(self):
        global algo, courseGraph

        self.assertEqual(algo.graph, courseGraph)
        self.assertCountEqual(algo.checkImplies(algo.checkPhi("req1"), algo.checkPhi("req1")), [node7])

    # MISSING TESTS :
    # EF(phi)
    # AF(phi)
    # EG(phi)
    # AG(phi)
    # phi <=> phi

#########################
#        [LEXER]       #
#########################

class TestLexer(unittest.TestCase):
    def testLexerIsAtomicPropWithBadInput(self):
        lexer: Lexer = Lexer("")

        self.assertFalse(lexer.isAtomicProp("#\*"))

    def testLexerIsAtomicPropWithValidAtomicProp(self):
        lexer: Lexer = Lexer("")

        self.assertTrue(lexer.isAtomicProp("req1"))

    def testLexerIsAtomicPropWithAnOperator(self):
        lexer: Lexer = Lexer("")

        self.assertFalse(lexer.isAtomicProp("OR"))

    def testLexerIsAtomicPropWithAnEmptyString(self):
        lexer: Lexer = Lexer("")

        self.assertFalse(lexer.isAtomicProp(""))

    def testTokenizerWithAnEmptyString(self):
        lexer: Lexer = Lexer("")

        self.assertEqual([], lexer.tokenizer())

    def testTokenizerWithAnEOF(self):
        lexer: Lexer = Lexer(None)

        self.assertEqual(Token("EOF", None), lexer.tokenizer())

    def testTokenizerWithAleftParenthese(self):
        lexer: Lexer = Lexer("(")

        self.assertEqual([Token("PARLEFT", "(")], lexer.tokenizer())

    def testTokenizerWithArightParenthese(self):
        lexer: Lexer = Lexer(")")

        self.assertEqual([Token("PARRIGHT", ")")], lexer.tokenizer())

    def testTokenizerWithAnAndOperator(self):
        lexer: Lexer = Lexer("AND")

        self.assertEqual([Token("AND", "AND")], lexer.tokenizer())

    def testTokenizerWithAnOrOperator(self):
        lexer: Lexer = Lexer("OR")

        self.assertEqual([Token("OR", "OR")], lexer.tokenizer())

    def testTokenizerWithAnotOperator(self):
        lexer: Lexer = Lexer("NOT")

        self.assertEqual([Token("NOT", "NOT")], lexer.tokenizer())

    def testTokenizerWithAnImpliesOperator(self):
        lexer: Lexer = Lexer("=>")

        self.assertEqual([Token("IMPLIES", "=>")], lexer.tokenizer())

    def testTokenizerWithAnEquivalentOperator(self):
        lexer: Lexer = Lexer("<=>")

        self.assertEqual([Token("EQUIVALENT", "<=>")], lexer.tokenizer())

    def testTokenizerWithAnextOperator(self):
        lexer: Lexer = Lexer("X")

        self.assertEqual([Token("NEXT", "X")], lexer.tokenizer())

    def testTokenizerWithAnAlwaysOperator(self):
        lexer: Lexer = Lexer("G")

        self.assertEqual([Token("ALWAYS", "G")], lexer.tokenizer())

    def testTokenizerWithAnOnePathOperator(self):
        lexer: Lexer = Lexer("E")

        self.assertEqual([Token("ONEPATH", "E")], lexer.tokenizer())

    def testTokenizerWithAfuturOperator(self):
        lexer: Lexer = Lexer("F")

        self.assertEqual([Token("FUTURE", "F")], lexer.tokenizer())

    def testTokenizerWithAnUntilOperator(self):
        lexer: Lexer = Lexer("U")

        self.assertEqual([Token("UNTIL", "U")], lexer.tokenizer())

    def testTokenizerWithAnAllPathOperator(self):
        lexer: Lexer = Lexer("A")

        self.assertEqual([Token("ALLPATH", "A")], lexer.tokenizer())

    def testTokenizerWithAtrueConstant(self):
        lexer: Lexer = Lexer("TRUE")

        self.assertEqual([Token("TRUE", "TRUE")], lexer.tokenizer())

    def testTokenizerWithAfalseConstant(self):
        lexer: Lexer = Lexer("FALSE")

        self.assertEqual([Token("FALSE", "FALSE")], lexer.tokenizer())

    def testTokenizerWithAnAtomicProp(self):
        lexer: Lexer = Lexer("req1")

        self.assertEqual([Token("ATOMIC", "req1")], lexer.tokenizer())

    def testTokenizerWithBasicCTLinput(self):
        lexer: Lexer = Lexer("E ( req1 U cs1 )")

        self.assertEqual([Token("ONEPATH", "E"), Token("PARLEFT", "("), Token("ATOMIC", "req1"), Token(
            "UNTIL", "U"), Token("ATOMIC", "cs1"), Token("PARRIGHT", ")")], lexer.tokenizer())

    def testTokenizerWithBadCTLinput(self):
        lexer: Lexer = Lexer("***************")

        self.assertEqual([], lexer.tokenizer())

#########################
#        [PARSER]       #
#########################

class TestParser(unittest.TestCase):
    """
    Case 1: E X ( req1 ) 
    Case 2: A X ( req1 ) 
    Case 3: E ( req1 U cs1 ) 
    Case 4: A ( req1 U cs1 ) 
    Case 5: E ( req1 U cs2 ) 
    Case 6: E F ( req1 ) 
    Case 7: A F ( req1 ) 
    Case 8: A X ( req1 AND req2 AND req3 AND req4 )
    Case 9: A G (  E F ( req1 AND req2 ) ) 
    Case 10: E G ( req1 )
    Case 11: A G ( req1 )
    Case 12: E ( cs1 => end1 )
    Case 13: A X ( req1 AND req2 )
    Case 14: ( A F ( req1 ) )
    """
    def testParserBuildAstCase1(self):
        lexer: Lexer = Lexer("E X ( req1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("NEXT", "X"),TreeNode(Token("ATOMIC", "req1"))),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase2(self):
        lexer: Lexer = Lexer("A X ( req1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ALLPATH", "A"),TreeNode(Token("NEXT", "X"),TreeNode(Token("ATOMIC", "req1"))),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase3(self):
        lexer: Lexer = Lexer("E ( req1 U cs1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("UNTIL", "U"),TreeNode(Token("ATOMIC", "req1")),TreeNode(Token("ATOMIC", "cs1"))),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase4(self):
        lexer: Lexer = Lexer("A ( req1 U cs1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ALLPATH", "A"),TreeNode(Token("UNTIL", "U"),TreeNode(Token("ATOMIC", "req1")),TreeNode(Token("ATOMIC", "cs1"))),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase5(self):
        lexer: Lexer = Lexer("E ( req1 U cs2 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("UNTIL", "U"),TreeNode(Token("ATOMIC", "req1")),TreeNode(Token("ATOMIC", "cs2"))),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase6(self):
        lexer: Lexer = Lexer("E F ( req1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("FUTURE", "F"),TreeNode(Token("ATOMIC", "req1")),None),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase7(self):
        lexer: Lexer = Lexer("A F ( req1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ALLPATH", "A"),TreeNode(Token("FUTURE", "F"),TreeNode(Token("ATOMIC", "req1")),None),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase8(self):
        lexer: Lexer = Lexer("A X ( req1 AND req2 AND req3 AND req4 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(root = Token("ALLPATH", "A"), left = TreeNode(root = Token("NEXT", "X"), left = TreeNode(root = Token("AND", "AND"), left = TreeNode(root = Token("AND", "AND"), left = TreeNode(root = Token("AND", "AND"), left = TreeNode(root = Token("ATOMIC", "req1"), left = None, right = None), right = TreeNode(root = Token("ATOMIC", "req2"), left = None, right = None)), right = TreeNode(root = Token("ATOMIC", "req3"), left = None, right = None)), right = TreeNode(root = Token("ATOMIC", "req4"), left = None, right = None)), right = None), right = None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase9(self):
        lexer: Lexer = Lexer("A G (  E F ( req1 AND req2 ) )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(root = Token("ALLPATH", "A"), left = TreeNode(root = Token("ALWAYS", "G"), left = TreeNode(root = Token("ONEPATH", "E"), left = TreeNode(root = Token("FUTURE", "F"), left = TreeNode(root = Token("AND", "AND"), left = TreeNode(root = Token("ATOMIC", "req1"), left = None, right = None), right = TreeNode(root = Token("ATOMIC", "req2"), left = None, right = None)), right = None), right = None), right = None), right = None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase10(self):
        lexer: Lexer = Lexer("E G ( req1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("ALWAYS", "G"),TreeNode(Token("ATOMIC", "req1")),None),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase11(self):
        lexer: Lexer = Lexer("A G ( req1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ALLPATH", "A"),TreeNode(Token("ALWAYS", "G"),TreeNode(Token("ATOMIC", "req1")),None),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase12(self):
        lexer: Lexer = Lexer("E ( cs1 => end1 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ONEPATH", "E"),TreeNode(Token("IMPLIES", "=>"),TreeNode(Token("ATOMIC", "cs1")),TreeNode(Token("ATOMIC", "end1"))),None)

        self.assertEqual(result,parser.buildAST())
    
    def testParserBuildAstCase13(self):
        lexer: Lexer = Lexer("A X ( req1 AND req2 )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ALLPATH", "A"),TreeNode(Token("NEXT", "X"),TreeNode(Token("AND", "AND"),TreeNode(Token("ATOMIC", "req1")),TreeNode(Token("ATOMIC", "req2"))),None),None)

        self.assertEqual(result,parser.buildAST())

    def testParserBuildAstCase14(self):
        lexer: Lexer = Lexer("( A F ( req1 ) )")
        parser: Parser = Parser(lexer.tokenizer())
        
        result: TreeNode = TreeNode(Token("ALLPATH", "A"),TreeNode(Token("FUTURE", "F"),TreeNode(Token("ATOMIC", "req1")),None),None)

        self.assertEqual(result,parser.buildAST())

#########################
#     [INTERPRETER]     #
#########################

class TestInterpreter(unittest.TestCase):
    """
    Case 1: E X ( req1 )
    Case 2: A X ( req1 )
    Case 3: E ( req1 U cs1 )
    Case 4: A ( req1 U cs1 )
    Case 5: E ( req1 U cs2 )
    Case 6: E F ( req1 )
    Case 7: A F ( req1 )
    Case 8: A X ( req1 AND req2 AND req3 AND req4 )
    Case 9: A G (  E F ( req1 AND req2 ) )
    Case 10: E G ( req1 )
    Case 11: A G ( req1 )
    Case 12: E ( cs1 => end1 )
    Case 13: A X ( req1 AND req2 )
    Case 14: ( A F ( req1 ) )
    """

    def testInterpreterCase1(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("ONEPATH", "E"), TreeNode(Token("NEXT", "X"), TreeNode(Token("ATOMIC", "req1"))), None)

        interpreter: Interpreter = Interpreter(treeNode, algo, "E X ( req1 )")
        self.assertCountEqual(interpreter.interpret(), [node0, node1, node2, node3, node5, node7])

    def testInterpreterCase3(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("ONEPATH", "E"), TreeNode(Token("UNTIL", "U"), TreeNode(Token("ATOMIC", "req1")), TreeNode(Token("ATOMIC", "cs1"))), None)

        interpreter: Interpreter = Interpreter(treeNode, algo, "E ( req1 U cs1 )")

        self.assertCountEqual(interpreter.interpret(), [node1, node3, node4, node6, node7])

    def testInterpreterCase4(self):
        global courseGraph
        
        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("ALLPATH", "A"), TreeNode(Token("UNTIL", "U"), TreeNode(Token("ATOMIC", "req1")), TreeNode(Token("ATOMIC", "cs1"))), None)

        interpreter: Interpreter = Interpreter(treeNode, algo, "A ( req1 U cs1 )")

        self.assertCountEqual(interpreter.interpret(), [node4, node6])

    """
    Fail cases:
        - Missing E/A logics
            - 1 : X ( req1 )
            - 2 : F ( req1 )
            - 3 : G ( req1 )
            - 4 : U ( req1 )

        - Missing temporal logics
            - 5 : req1
            - 6 : NOT req1
            - 7 : req1 AND req2
            - 8 : req1 OR req2
            - 9 : req1 => cs1
    """

    def testInterpreterFail1(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("NEXT", "X"), TreeNode(Token("ATOMIC", "req1")), None)
        formula = "X ( req1 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_RAISEERROR'].format(formula) in str(context.exception))

    def testInterpreterFail2(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("FUTURE", "F"), TreeNode(Token("ATOMIC", "req1")), None)
        formula = "F ( req1 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_RAISEERROR'].format(formula) in str(context.exception))

    def testInterpreterFail3(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("ALWAYS", "G"), TreeNode(Token("ATOMIC", "req1")), None)
        formula = "G ( req1 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_RAISEERROR'].format(formula) in str(context.exception))

    def testInterpreterFail4(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("UNTIL", "U"), TreeNode(Token("ATOMIC", "req1")), TreeNode(Token("ATOMIC", "cs1")))
        formula = "( req1 U cs1 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_RAISEERROR'].format(formula) in str(context.exception))

    def testInterpreterFail5(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("ATOMIC", "req1"), None, None)
        formula = "( req1 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_ATOMIC_ERROR'].format(formula) in str(context.exception))

    def testInterpreterFail6(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("NOT", "NOT"), Token("ATOMIC", "req1"), None)
        formula = "( NOT req1 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_ATOMIC_ERROR'].format(formula) in str(context.exception))

    def testInterpreterFail7(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("AND", "AND"), Token("ATOMIC", "req1"), Token("ATOMIC", "req2"))
        formula = "( req1 AND req2 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_ATOMIC_ERROR'].format(formula) in str(context.exception))

    def testInterpreterFail8(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("OR", "OR"), Token("ATOMIC", "req1"), Token("ATOMIC", "req2"))
        formula = "( req1 OR req2 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_ATOMIC_ERROR'].format(formula) in str(context.exception))

    def testInterpreterFail9(self):
        global courseGraph

        algo: Algo = Algo(courseGraph)
        treeNode = TreeNode(Token("IMPLIES", "=>"), Token("ATOMIC", "req1"), Token("ATOMIC", "req2"))
        formula = "( req1 => req2 )"
        interpreter: Interpreter = Interpreter(treeNode, algo, formula)

        with self.assertRaises(ValueError) as context:
            interpreter.interpret()

        self.assertTrue(DEFAULT["MSG"]['INTERPRETEROBJ_VISIT_ATOMIC_ERROR'].format(formula) in str(context.exception))


if __name__ == "__main__":
    unittest.main()

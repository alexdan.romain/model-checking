import unittest

import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from tests import (
    TestDeserialization,
    TestIsFirstLineValid,
    TestNode,
    TestGraph,
    TestAlgo,
    TestIsSecondLineValid,
    TestIsThirdLineValid,
    TestIsForthLineValid,
    TestLexer,
    TestParser,
    TestInterpreter,
)

suiteDeserialization = unittest.TestLoader().loadTestsFromTestCase(TestDeserialization)
suiteIsFirstLine = unittest.TestLoader().loadTestsFromTestCase(TestIsFirstLineValid)
suiteIsSecondLine = unittest.TestLoader().loadTestsFromTestCase(TestIsSecondLineValid)
suiteIsThirdLine = unittest.TestLoader().loadTestsFromTestCase(TestIsThirdLineValid)
suiteIsForthLine = unittest.TestLoader().loadTestsFromTestCase(TestIsForthLineValid)
suiteNode = unittest.TestLoader().loadTestsFromTestCase(TestNode)
suiteGraph = unittest.TestLoader().loadTestsFromTestCase(TestGraph)
suiteAlgo = unittest.TestLoader().loadTestsFromTestCase(TestAlgo)
suiteLexer = unittest.TestLoader().loadTestsFromTestCase(TestLexer)
suiteParser = unittest.TestLoader().loadTestsFromTestCase(TestParser)
suiteInterpreter = unittest.TestLoader().loadTestsFromTestCase(TestInterpreter)

all_tests = unittest.TestSuite(
    [
        suiteDeserialization,
        suiteIsFirstLine,
        suiteNode,
        suiteGraph,
        suiteAlgo,
        suiteIsSecondLine,
        suiteIsThirdLine,
        suiteIsForthLine,
        suiteLexer,
        suiteParser,
        suiteInterpreter,
    ]
)

unittest.TextTestRunner().run(all_tests)

import unittest

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils import deserialization,isFirstLineValid,isSecondLineValid,isThirdLineValid,isForthLineValid
from models.graph import Graph,Node

class TestDeserialization(unittest.TestCase):

    def testDeserializationWithWrongFirstLine(self):
        with self.assertRaises(ValueError):
            deserialization(["1 r\n","1 r\n","1 r\n","1 r\n"])
    
    def testDeserializationWithWrongSecondLine(self):
        with self.assertRaises(ValueError):
            deserialization(["1 1\n","1 r\n","1 r\n","1 r\n"])
    
    def testDeserializationWithWrongThirdLine(self):
        with self.assertRaises(ValueError):
            deserialization(["1 1\n","1 1 a\n","1 r\n","1 r\n"])
    
    def testDeserializationWithWrongForthLine(self):
        with self.assertRaises(ValueError):
            deserialization(["1 1\n","1 1 a\n","1\n","1 r\n"])
    
    def testDeserializationWithBasicInput(self):
        result: Graph = Graph()
        node : Node = Node("1")
        
        node.add_atomicProp("a")
        
        result.add_atomicProp("a")
        result.add_node(node)
        result.add_initial(node)
        
        self.assertEqual(result,deserialization(["1 1\n","1 1 a\n","1\n","1 1\n"]))     
    
class TestIsFirstLineValid(unittest.TestCase):
    def testFirstLineWithBadInput(self):
        self.assertFalse(isFirstLineValid("a a"))
    
    def testFirstLineWithAnEmptyString(self):
        self.assertFalse(isFirstLineValid(""))
    
    def testFirstLineWithtooMuchArgs(self):
        self.assertFalse(isFirstLineValid("1 2 3 4"))
        
    def testFirstLineWithNotEnoughtArgs(self):
        self.assertFalse(isFirstLineValid("1"))
    
    def testFirstLineWithGoodInput(self):
        self.assertTrue(isFirstLineValid("1 2"))
    
    def testFirstLineWithMultipleSpaceInInput(self):
        self.assertFalse(isFirstLineValid("1   2"))

class TestIsSecondLineValid(unittest.TestCase):
    def testSecondLineWithBadInput(self):
        self.assertFalse(isSecondLineValid("# 1"))
    
    def testSecondLineWithAnEmptyString(self):
        self.assertFalse(isSecondLineValid(""))
    
    def testSecondLineWithtooMuchArgs(self):
        self.assertFalse(isSecondLineValid("1 2 3 4"))
        
    def testSecondLineWithNotEnoughtArgs(self):
        self.assertFalse(isSecondLineValid("1"))
    
    def testSecondLineWithGoodInput(self):
        self.assertTrue(isSecondLineValid("s1 2 p"))
    
    def testSecondLineWithMultipleSpaceInInput(self):
        self.assertFalse(isSecondLineValid("s1   2"))
    
    def testSecondLineWithSpecialsChar(self):
        self.assertFalse(isSecondLineValid("s#1 2"))

class TestIsThirdLineValid(unittest.TestCase):
    def testThirdLineWithBadInput(self):
        self.assertFalse(isThirdLineValid("-"))
    
    def testThirdLineWithAnEmptyString(self):
        self.assertFalse(isThirdLineValid(""))
    
    def testThirdLineWithtooMuchArgs(self):
        self.assertFalse(isThirdLineValid("1 2"))
        
    def testThirdLineWithNotEnoughtArgs(self):
        self.assertFalse(isThirdLineValid(" "))
    
    def testThirdLineWithGoodInput(self):
        self.assertTrue(isThirdLineValid("s1"))
    
    def testThirdLineWithMultipleSpaceInInput(self):
        self.assertFalse(isThirdLineValid("s1   "))
    
    def testThirdLineWithSpecialsChar(self):
        self.assertFalse(isThirdLineValid("#"))

class TestIsForthLineValid(unittest.TestCase):
    def testForthLineWithBadInput(self):
        self.assertFalse(isForthLineValid("- -"))
    
    def testForthLineWithAnEmptyString(self):
        self.assertFalse(isForthLineValid(""))
    
    def testForthLineWithtooMuchArgs(self):
        self.assertFalse(isForthLineValid("1 2 e"))
        
    def testForthLineWithNotEnoughtArgs(self):
        self.assertFalse(isForthLineValid("1"))
    
    def testForthLineWithGoodInput(self):
        self.assertTrue(isForthLineValid("s1 s2"))
    
    def testForthLineWithMultipleSpaceInInput(self):
        self.assertFalse(isForthLineValid("s1   s2"))
    
    def testForthLineWithSpecialsChar(self):
        self.assertFalse(isForthLineValid("# #"))

if __name__ == '__main__':
    unittest.main()
